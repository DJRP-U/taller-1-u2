import DataTable from 'react-data-table-component';
import { Autos } from '../hooks/Conexion';
import { borrarSesion } from '../utilidades/Sessionutils';
import { useNavigate } from 'react-router-dom';
import { useEffect, useState } from 'react';
import Mensajes from '../utilidades/Mensajes';
// A super simple expandable component.
const PresentarTablaAuto = () => {
    const [selectedRow, setSelectedRow] = useState(null);
    const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
    const navegation = useNavigate();
    const [data, setData] = useState([]);

    const handleRowClick = (row) => {
        setSelectedRow(row);
    };

    const handleCloseModal = () => {
        setSelectedRow(null);
    };

    const handleSaveChanges = (modifiedData) => {
        
        handleCloseModal();
    };

    useEffect(() => {
        Autos().then((info) => {
            console.log(info);
            if (info.code != 200 || info.msg == 'Acceso denegado. Token a expirado') {
                borrarSesion();
                Mensajes(info.msg);
                navegation("/sesion")
            } else {
                setData(info.info);
                console.log(info.info);
            }
        });
    }, []);


    const columns = [
        {
            name: 'Modelo',
            selector: row => row.modelo,
        },
        {
            name: 'Color',
            selector: row => row.color,
        },
        {
            name: 'Año',
            selector: row => row.anio,
        }, {
            name: 'Motor',
            selector: row => row.motor,
        }, {
            name: 'Precio',
            selector: row => '$' + row.precio,
        }
    ];
    return (
        <div>
            <DataTable
                columns={columns}
                data={data}
                onRowDoubleClicked={handleRowClick}
            />

            {selectedRow && (
                <ModalForm
                    rowData={selectedRow}
                    handleClose={handleCloseModal}
                    handleSave={handleSaveChanges}
                />
            )}
        </div>


    );
}

const ModalForm = ({ rowData, handleClose, handleSave }) => {
    const [modifiedData, setModifiedData] = useState(rowData);
  
    const handleChange = (e) => {
      const { name, value } = e.target;
      setModifiedData((prevData) => ({
        ...prevData,
        [name]: value,
      }));
    };
  
    const handleSubmit = (e) => {
      e.preventDefault();
      handleSave(modifiedData);
    };
  
    return (
      <div className="modal">
        <div className="modal-content">
          <h2>Modificar Datos</h2>
          <form onSubmit={handleSubmit}>
            <label>
              Modelo:
              <input
                type="text"
                name="modelo"
                value={modifiedData.modelo}
                onChange={handleChange}
              />
            </label>
            <label>
              Color:
              <input
                type="text"
                name="color"
                value={modifiedData.color}
                onChange={handleChange}
              />
            </label>
            {/* Agrega los demás campos del formulario según tus necesidades */}
            <div>
              <button type="submit">Guardar Cambios</button>
              <button onClick={handleClose}>Cancelar</button>
            </div>
          </form>
        </div>
      </div>
    );
  };

export default PresentarTablaAuto;