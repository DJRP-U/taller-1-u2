import { useNavigate } from 'react-router';
import { AutosCantidad, Marca } from '../hooks/Conexion';
import { borrarSesion, getToken } from '../utilidades/Sessionutils';
import Footer from './Footer';
import Header from "./Header";
import mensajes from '../utilidades/Mensajes';
import { useState } from 'react';


const Inicio = () => {
    const navegation = useNavigate();
    const [nro, setNro] = useState(0);
    const [nroA, setNroA] = useState(0);
    Marca(getToken()).then((info) => {
        if (info.error === true && info.messaje === 'Acceso denegado. Token ha expirado') {
            borrarSesion();
            mensajes(info.msg);
            navegation("/sesion")
        } else {
            setNro(info.length);
        }
    })
    //const autos = Autos(getToken());
    AutosCantidad(getToken()).then((info) => {
        if (info.error === true && info.code !== 200) {
            borrarSesion();
            mensajes(info.msg);
            navegation("/sesion")
        } else {
            setNroA(info.info);
            console.log(nroA)
        }
    })
    return (
        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">
                    <Header />
                    <div className='content-fluid'>
                        <h1 className="h3 mb-0 text-gray-800">Dashboard</h1>
                        <div className="row">

                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-left-primary shadow h-100 py-2">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                    NUmero de marcas</div>
                                                <div className="h5 mb-0 font-weight-bold text-gray-800">{nroA}</div>
                                            </div>
                                            <div className="col-auto">
                                                <i className="fas fa-calendar fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-left-success shadow h-100 py-2">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                    Earnings (Annual)</div>
                                                <div className="h5 mb-0 font-weight-bold text-gray-800">$215,000</div>
                                            </div>
                                            <div className="col-auto">
                                                <i className="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <button onClick={borrarSesion.then}>Borrar token</button>
            <Footer />
        </div>
    );
}

export default Inicio;