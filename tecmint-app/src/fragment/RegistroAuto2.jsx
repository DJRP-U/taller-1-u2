import React, { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import { GuardarAuto, ObtenerMarcas } from '../hooks/Conexion';
import axios from 'axios';
import { getToken } from '../utilidades/Sessionutils';
import Mensajes from '../utilidades/Mensajes';
import { Navigate } from 'react-router';
const URL = "http://127.0.0.1:3007/api";

const FormularioPost = () => {
  const [marca, setMarca] = useState([]);
  const [formulario, setFormulario] = useState({
    external_marca: '',
    modelo: '',
    color: '',
    anio: '',
    tipo: '',
    motor: '',
    detalles: '',
    precio: '',
  });

  useEffect(() => {
    cargarMarca();
  }, []);

  const cargarMarca = async () => {
    try {

        ObtenerMarcas(getToken()).then((response) => {
            console.log(response.info);
             setMarca(response.info);
        });
      
    } catch (error) {
      console.error(error);
    }
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormulario({ ...formulario, [name]: value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
    console.log(formulario);
    console.log(getToken());    
      GuardarAuto(formulario,getToken()).then((info)=>{
        if (info.code === 401) {
            Mensajes("Su sesion ya no es valida, inicie sesion nuevamente","error","Sesion invalidad");
        }
        if(info.code !== 401){
            Mensajes("Hubo un error al guardar el auto: "+info.msg,"error","No se pudo guardar");
        }
      }
      )
      // Realizar acciones adicionales después del envío exitoso
    } catch (error) {
      console.error(error);
      // Realizar acciones en caso de error en la solicitud
    }
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Form.Group controlId="externalMarca">
        <Form.Label>Marca</Form.Label>
        <Form.Control
          as="select"
          name="external_marca"
          onChange={handleChange}
          value={formulario.external_marca}
        >
          {marca.map((marca) => (
            <option key={marca.external_id} value={marca.external_id}>
              {marca.Nombre}
            </option>
          ))}
        </Form.Control>
      </Form.Group>

      <Form.Group controlId="modelo">
        <Form.Label>Modelo</Form.Label>
        <Form.Control
          type="text"
          name="modelo"
          onChange={handleChange}
          value={formulario.modelo}
        />
        <Form.Control.Feedback type="invalid">
            Por favor, proporcione un modelo.
          </Form.Control.Feedback>
      </Form.Group>

      <Form.Group controlId="color">
        <Form.Label>Color</Form.Label>
        <Form.Control
          type="text"
          name="color"
          onChange={handleChange}
          value={formulario.color}
        />
        <Form.Control.Feedback type="invalid">
            Por favor, proporcione una color.
          </Form.Control.Feedback>
      </Form.Group>

      <Form.Group controlId="anio">
        <Form.Label>Año</Form.Label>
        <Form.Control
          type="number"
          name="anio"
          onChange={handleChange}
          value={formulario.anio}
        />
        <Form.Control.Feedback type="invalid">
            Por favor, proporcione un año valido.
          </Form.Control.Feedback>
      </Form.Group>

      <Form.Group controlId="tipo">
        <Form.Label>Tipo</Form.Label>
        <Form.Control
          as="select"
          name="tipo"
          onChange={handleChange}
          value={formulario.tipo}
        >
          <option value="2X$">2X$</option>
          <option value="4X4">4X4</option>
        </Form.Control>
        <Form.Control.Feedback type="invalid">
            Por favor, proporcione un tipo.
          </Form.Control.Feedback>
      </Form.Group>

      <Form.Group controlId="motor">
        <Form.Label>Motor</Form.Label>
        <Form.Control
          as="select"
          name="motor"
          onChange={handleChange}
          value={formulario.motor}
        >
          <option value="GASOLINA">Gasolina</option>
          <option value="DIESEL">Diesel</option>
          <option value="ELECTRICO">Eléctrico</option>
          <option value="HIDROGENO">Hidrógeno</option>
        </Form.Control>
        <Form.Control.Feedback type="invalid">
            Por favor, proporcione un motor.
          </Form.Control.Feedback>
      </Form.Group>

      <Form.Group controlId="detalles">
        <Form.Label>Detalles</Form.Label>
        <Form.Control
          as="textarea"
          name="detalles"
          onChange={handleChange}
          value={formulario.detalles}
        />
      </Form.Group>

      <Form.Group controlId="precio">
        <Form.Label>Precio</Form.Label>
        <Form.Control
          type="number"
          name="precio"
          onChange={handleChange}
          value={formulario.precio}
        />
      </Form.Group>

      <Button variant="primary" type="submit">
        Enviar
      </Button>
    </Form>
  );
};

export default FormularioPost;
