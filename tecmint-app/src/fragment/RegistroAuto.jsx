import React, { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import { FormControl, DropdownButton, Dropdown } from 'react-bootstrap';
import InputGroup from 'react-bootstrap/InputGroup';
import Row from 'react-bootstrap/Row';
import { estaSesion, getToken } from '../utilidades/Sessionutils';
import { GuardarAuto } from '../hooks/Conexion';
import Mensajes from '../utilidades/Mensajes';
import { useNavigate } from 'react-router';
const URL = "http://127.0.0.1:3007/api"

function RegistroAuto() {
  const [validated, setValidated] = useState(false);
  const [data, setData] = useState([]);
  const [selectedValue, setSelectedValue] = useState('');
  const [selectedItem, setSelectedItem] = useState(null);


  useEffect(() => {
    fetch(URL + "/marca")
      .then(response => response.json())
      .then(jsonData => {
        setData(jsonData.info);
      })
      .catch(error => {
        console.error('Error:', error);
      });
  }, []);

  const handleSubmit = (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }

    setValidated(true);
  };
  const onSubmit = (data) => {

    var datos = {
      "external_marca": data.marca,
      "modelo": data.modelo,
      "anio": data.anio,
      "tipo": data.tipo,
      "motor": data.motor,
      "precio": data.precio,
      "color": data.color
    };
    if (estaSesion) {
      console.log(getToken())
      console.log(datos)
      GuardarAuto(datos, getToken()).then((info) => {
        if (info.code !== 200) {
          //console.log(info);
          Mensajes(info.msg, 'error', 'Error');
          //msgError(info.message);            
        } else {
          Mensajes(info.msg);
        }
      }
      );
    }
    /*
    InicioSesion(datos).then((info) => {
      console.log('SessionFRM');
      //console.log(info);
      if (info.msg !== 200) {
        Mensajes(info.message)
        //alert(info.message);
      } else {
        saveToken(info.data.token);
        Mensajes(info.message, 'success');
        navigation('/inicio');
        console.log(info.data); //Recibe los datos del json como clase
        console.log(info.data.token);
      }
    });
    */


  }
  return (
    <Form noValidate validated={validated} onSubmit={handleSubmit}>
      <Row className="mb-3">
        <Form.Group as={Col} md="4" controlId="validationCustom00">
          <Form.Label>Año</Form.Label>
          <Form.Control type="text" placeholder="2002" required />
          <Form.Control.Feedback type="invalid">
            Por favor, proporcione una año.
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group as={Col} md="4" controlId="validationCustom01">
          <Form.Label>Color</Form.Label>
          <Form.Control type="text" placeholder="Rojo" required />
          <Form.Control.Feedback type="invalid">
            Por favor, proporcione un color.
          </Form.Control.Feedback>
        </Form.Group>
      </Row>
      <Row className="mb-3">
        <Form.Group as={Col} md="3" controlId="validationCustom02">
          <Form.Label>Precio</Form.Label>
          <Form.Control type="text" placeholder="00000.00" required />
          <Form.Control.Feedback type="invalid">
            Por favor, proporcione un precio.
          </Form.Control.Feedback>
        </Form.Group>
      </Row>
      <Form.Group as={Col} md="4" controlId="validationCustom03">
        <Form.Label>Marca</Form.Label>
        <Form>
          <Dropdown>
            <Dropdown.Toggle variant="primary" id="dropdown-basic">
              {selectedItem ? selectedItem : "Seleccionar nombre"}
            </Dropdown.Toggle>
            <Dropdown.Menu>
              {data.map(item => (
                <Dropdown.Item
                key={item.external_id}
                onClick={() => setSelectedItem(item.Nombre)}
                >
                {item.Nombre}
              </Dropdown.Item>
              ))}
            </Dropdown.Menu>
          </Dropdown>
        </Form>
      </Form.Group>
      <Button className="btn btn-success mt-4" type="submit">Registrar auto</Button>
    </Form>
  );
}

export default RegistroAuto;