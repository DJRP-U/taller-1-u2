const URL = "http://127.0.0.1:3007/api"
export const InicioSesion = async (data) => {
    /*    const config = {
            headers:{"Content-Type" : "application/json"}
        };
    */
    /*await axios.post(URL+"/sesion", data, config).then((response)=>{
        console.log(response);
        return response;
    }, error=>{
        console.log('Error');
        console.log(Error);
    }
    );*/
    const headers = {
        'Accept':'application/json',
        "Content-Type": 'application/json'
    }
    const datos = await (await fetch(URL + "/sesion", {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    })).json();
    console.log(datos);
    return datos;

}
export const Marca = async (key) => {

    var cabecera = {

        "x-api-token": key

    }

    const datos = await (await fetch(URL + "/marca", {
        method: "GET",
        headers: cabecera
    })).json();
    console.log(datos)
    return datos;

}
export const Autos = async (key) => {

    var cabecera = {

        "x-api-token": key
    }
    const datos = await (await fetch(URL + "/auto/sinDuenio", {
        method: "GET",
        headers: cabecera
    })).json();
    return datos;

}
export const AutosCantidad = async(key) => {
    var cabeceras = { "x-api-token": key };
    const datos = await (await fetch(URL + "/auto/contar", {
        method: "GET",
        headers: cabeceras
    })).json();
    return datos;
}

export const ObtenerMarcas = async(key) => {
    var cabeceras = { "x-api-token": key };
    const datos = await (await fetch(URL + "/marca", {
        method: "GET",
        headers: cabeceras
    })).json();
    return datos;
}

export const ObtenerColores= async()=>{
    const datos= await (await fetch(URL + "/auto/colores",{
        method: "GET",

     })).json();
     console.log(datos);
     return datos;
}
export const GuardarAuto = async (data,key) => {
    var cabeceras = { "x-api-token": key };
    const datos = await (await fetch(URL + "/auto/guardar", {
        method: "POST",
        headers: cabeceras,
        body: JSON.stringify(data)
    })).json();
    return datos;

}
export const ModificarAuto = async (data,key) => {
    var cabeceras = { "x-api-token": key };
    const datos = await (await fetch(URL + "/auto/modificar", {
        method: "PUT",
        headers: cabeceras,
        body: JSON.stringify(data)
    })).json();
    return datos;

}