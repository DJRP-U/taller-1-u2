var express = require('express');
var router = express.Router();
const { body, validatioResult } = require('express-validator');
const RolController = require('../controls/RolController');
const AutoController = require('../controls/AutoController');
const PersonaController = require('../controls/PersonaController');
const VentaController = require('../controls/VentaController');
const CuentaController = require('../controls/CuentaController');
const MarcaController = require('../controls/MarcaController');
let jwt = require('jsonwebtoken');
var autoController = new AutoController();
var rolController = new RolController();
var personaController = new PersonaController();
var ventaController = new VentaController();
var cuentaController = new CuentaController();
var marcaController = new MarcaController();

var auth = function middlware(req, res, next) {
  //tipos de identificacion 
  const token = req.headers['x-api-token'];
  if (token) {
    require("dotenv").config();
    const llave = process.env.KEY;
    jwt.verify(token, llave, async (err, decoded) => {
      if (err) {
        res.status(401);
        res.json({ msg: "Token no valido!", code: 401 });
      } else {
        //
        var models = require('../models');
        var cuenta = models.cuenta;
        req.decoded = decoded;
        console.log(req.decoded);
        let aux = await cuenta.findOne({ where: { external_id: req.decoded.external} });
        if (aux === null) {
          res.status(401);
          res.json({ msg: "Token no valido!", code: 401 });
        } else {
          next();
        }

      }
    });
  } else {
    res.status(401);
    res.json({ msg: "No existe token o token invalido!", code: 401 });
  }
}

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.json(
    {
      "version": "1.0",
      "name": "auto"
    }
  );
});
router.get('/roles',auth, rolController.listar);
router.get('/marca',auth, marcaController.listar);
router.get('/auto/sinDuenio',auth, autoController.listarSinDuenio);
router.get('/auto/conDuenio',auth, autoController.listarConDuenio);
router.get('/auto/contar',auth, autoController.contar);
router.put('/auto/modificar', autoController.modificar,[
  body('external_auto', 'Ingrese la llave externa de auto').exists().not().isEmpty().withMessage('Ingrese la llave externa de auto')
]);
router.get('/venta/listar',auth, ventaController.listar);
router.get('/cuenta/listar',auth, cuentaController.listar);
router.get('/suma/:a/:b', function (req, res, next) {
  var a = req.params.a * 1;
  var b = Number(req.params.b);
  var c = a + b;
  res.status(200);
  res.json(
    {
      "msg": "OK",
      "resp": c
    }
  );
});
router.get('/persona/obtener/:external', function (req, res, next) {
});

router.get('/persona/listar', personaController.listar);

router.get('/auto/obtener/:external', autoController.obtener);

router.post('/sesion', cuentaController.sesion);

router.post('/personas/guardar', [
  body('apellidos', 'Ingrese datos').exists().not().isEmpty().withMessage("Ingrese su apellido"),
  body('nombres', 'Ingrese datos').exists(),
  body('external_rol', 'Ingrese rol').exists().not().isEmpty().withMessage('Ingrese un rol')
], personaController.guardar);

//guardar datos de auto
router.post('/auto/guardar',auth, [
  body('modelo', 'Ingrese datos'),
  body('external_marca', 'Ingrese el external_id de marca').exists().not().isEmpty().withMessage('Ingrese un external_id')
], autoController.guardar);

//guardar datos de venta
router.post('/venta/guardar', [
  body('external_persona', 'Ingrese datos').exists().not().isEmpty().withMessage("Ingrese el external_id de persona"),
  body('external_auto', 'Ingrese datos').exists().not().isEmpty().withMessage("Ingrese el external_id de auto")
], ventaController.guardar);

router.post('/sumar', function (req, res, next) {
  var a = req.body.a * 1;
  var b = Number(req.body.b);
  console.log(b);
  if ((isNaN(a) || isNaN(b))) {
    res.status(400);
    res.json(
      {
        "msg": "Error",
        "resp": "Faltan datos"
      });
  } else {
    var c = a + b;
    res.status(200);
    res.json(
      {
        "msg": "OK",
        "resp": c
      }
    );
  }

});
module.exports = router;  
