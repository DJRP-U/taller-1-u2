import { Container } from "react-bootstrap";
import Footer from "./Footer";
import Header from "./Header";
import { useNavigate } from "react-router";
import { Marcas } from "../hooks/Conexion";
import { getToken } from "../utilidades/Sessionutils";
import { useState } from "react";
const Inicio = () => {
    const Navigation = useNavigate();
    const [nro, setNro] = useState(0);  
    return ( 
        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">
                    <Header/> 
                    <Container/>
                </div>
            </div>
        <Footer/>
        </div>
     );
}
 
export default Inicio;