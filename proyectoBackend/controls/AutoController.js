'use strict';
const { body, validationResult, check } = require('express-validator');
const { Op, sequelize } = require('sequelize');
var models = require('../models/');
var persona = models.persona;
var venta = models.venta;
var auto = models.auto;
var marca = models.marca;
const bcrypt = require('bcrypt');
const salRounds = 8;
class AutoController {
    async listarSinDuenio(req, res) {
        try {
            const listar = await auto.findAll({
                attributes: ['modelo','color','anio','tipo','motor','detalles','precio','external_id'],
                where : {
                    '$venta.id$': null
                },
                include:[
                    {
                        model: venta,
                        attributes: [],
                        required: false
                    }
                ],
            })
            res.status(200);
            res.json({ msg: "0k", code: 200, info: listar });
        } catch (error) {
            console.error(error);
            res.status(500).json({ message: 'Error al obtener los autos disponibles' });
        }
    }

    async contar(req, res) {
        var listar = await auto.count();
        res.status(200);
        res.json({ msg: "Obtenido numero de autos!", code: 200, info: listar });

    }

    async obtener(req, res) {
        const external = req.params.external;
        try {findAll
            var lista = await auto.findOne({
                where: { external_id: external },
                attributes: ['modelo','color','anio','tipo','motor','detalles','precio']
            });
            if (lista == null) {
                lista = {
                    msg: "Sin auto"
                };
            }
            res.status(200);
            res.json({ msg: "OK!", code: 200, info: lista });
        } catch (error) {
            console.error(error);
            res.status(500).json({ message: 'Error al obtener el auto solicitado' });
        }
    }

    async listarConDuenio(req, res) {
        try {
            const listar = await auto.findAll({
                attributes: ['modelo','color','anio','tipo','motor','detalles','precio','external_id'],
                where : {
                    '$venta.id$': {
                        [Op.not]: null
                    }
                },
                include:[   
                    {
                        model: venta,
                        attributes: [],
                        required: true
                    }
                ],
            })
            res.status(200);
            res.json({ msg: "0k", code: 200, info: listar });
        } catch (error) {
            console.error(error);
            res.status(500).json({ message: 'Error al obtener los autos disponibles' });
        }
    }

    async guardar(req, res) {
        
            let errors = validationResult(req);
            if (errors.isEmpty()) {
                const marcaId= req.body.external_marca;

                if (1===1) {
                    let marcaAux = await marca.findOne({ where: { external_id: marcaId } });
                    if (marcaAux) {
                        //data arreglo asociativo= es un direccionario = clave:valor
                        var data = {
                            modelo: req.body.modelo,
                            color: req.body.color,
                            anio: req.body.anio,
                            tipo: req.body.tipo,
                            motor: req.body.motor,
                            detalles: req.body.detalles,
                            precio: req.body.precio,
                            id_marca: marcaAux.id
                        }
                        try {
                            await auto.create(data);
                            res.json({ msg: "Se han registrado los datos", code: 200 });
                        } catch (error) {
                            if (error.errors && error.errors[0].message) {
                                res.status(400);
                                res.json({ msg: error.errors[0].message, code: 400 });
                            } else {
                                res.status(400);
                                res.json({ msg: error.message, code: 400 });
                            }
                        }
                    } else {
                        res.status(400);
                        res.json({ msg: "Datos no encontrados", code: 400 });
                    }
                } else {
                    res.status(400);
                    res.json({ msg: "Faltan datos", code: 400 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Datos faltantes", code: 400, errors: errors });
            }
    }

    async modificar(req, res) {
        if (!req.body.external_auto) {
            req.body.external_auto = "INDEFINIDO"
        }
        var autom = await auto.findOne({ where: { external_id: req.body.external_auto } });
        if (autom === null) {
            res.json({ msg: "No existe el registro de auto", code: 400 });
        } else {
            autom.modelo = req.body.modelo;
            autom.color = req.body.color;
            autom.anio = req.body.anio;
            autom.tipo = req.body.tipo;
            autom.motor = req.body.motor;
            autom.detalles = req.body.detalles;
            var result = await autom.save();
            if (result === null) {

            } else {
                res.status(200);
                res.json({ msg: "se han modificado sus datos", code: 200 });
            }
        }
    }

}
module.exports = AutoController;