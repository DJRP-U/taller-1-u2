'use strict';
module.exports = (sequalize, DataTypes) => {
    const marca = sequalize.define('marca', {

        Nombre: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        Convenio: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
            defaultValue: false
        },
        external_id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4},
    }, {freezeTableName: true});
    marca.associate = function(models) {
        marca.hasMany(models.auto, {foreignKey: 'id_marca', as: 'marca'});
    }
    return marca;
}