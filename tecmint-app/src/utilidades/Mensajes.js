import swal from "sweetalert";
const Mensajes = (texto,type='error',title='Error') => swal({
            title: title,
            text: texto,
            icon: type,
            button: 'OK',
            timer: 200000,
            closeOnEsc: true
          });
 
export default Mensajes;