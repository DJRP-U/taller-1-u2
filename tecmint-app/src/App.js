import React, { Children } from 'react';
import './App.css';
import {Navigate, Route, Routes, useLocation} from 'react-router-dom';
import Sesion from './fragment/Sesion';
import Inicio from './fragment/Inicio';
import Crud from './fragment/Crud';
import Inicio2 from './fragment/Inicio2';
import Principal from './fragment/Principal';
import { estaSesion } from './utilidades/Sessionutils';

function App() {

  const Middleware = ({children}) => {
    const autenticado = estaSesion();
    const location = useLocation();
    if(autenticado){
      return children
    } else {
      return <Navigate to='/Sesion' state={location}/>
    }

  }
  const MiddlewareSession = ({children}) => {
    const autenticado = estaSesion();
    const location = useLocation();
    if(autenticado){
      return <Navigate to='/Sesion'/>
    } else {
      return children;
    }

  }
  return (
    <div className="App">
      <Routes>
      <Route path='/' element={<MiddlewareSession><Principal/></MiddlewareSession>}exact/>
      <Route path='/sesion' element={<Sesion/>}/>
      <Route path='/inicio' element={<Middleware><Inicio2/></Middleware>}/>
      <Route path='/auto' element={<Middleware><Crud/></Middleware>}/>
      <Route path='/principal' element={<Principal/>}/>
      </Routes>
      </div>
  );
}

export default App;
